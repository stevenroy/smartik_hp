<?php
   include('connect.php');
   error_reporting(E_ALL ^ E_NOTICE);

$output = '';
if(isset($_POST["query"]))
{
 $search = mysqli_real_escape_string($connect, $_POST["query"]);
 $query = "
  SELECT a.Kode_hp, a.Nama_series, a.Harga, a.luncur, b.Asal_buatan FROM tbl_series a, tbl_handphone b
  WHERE b.Id_hp = a.Id_hp AND a.Populer > 6 AND a.Nama_series LIKE '%".$search."%' 
 ";
}
else
{
 $query = "
  SELECT a.Kode_hp, a.Nama_series, a.Harga, a.luncur, b.Asal_buatan FROM tbl_series a, tbl_handphone b
  WHERE b.Id_hp = a.Id_hp AND a.Populer > 6 ORDER BY a.Kode_hp
 ";
}
$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
 $output .= '
  <div class="table-responsive">
   <table class="table table bordered">
    <tr>
     <th>Series</th>
     <th>Harga</th>
     <th>Release</th>
     <th>Made in</th>
    </tr>
 ';
 while($row = mysqli_fetch_array($result))
 {
  $output .= '
   <tr>
   <td><a href="detail.php?ID='.$row["Kode_hp"].'">'.$row["Nama_series"].'</a></td>
    <td>'.$row["Harga"].'</td>
    <td>'.$row["luncur"].'</td>
    <td>'.$row["Asal_buatan"].'</td>
   </tr>
  ';
 }
 echo $output;
}
else
{
    echo 'Pencarian <b>"' .$search. '"</b> tidak ditemukan';
}

?>